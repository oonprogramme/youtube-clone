import 'package:custom/videoComponent.dart';
import 'package:custom/videoModel.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Youtube Clone',
      theme: ThemeData(
        splashColor: Colors.black,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<VideoModel> getVideo() {
    List<VideoModel> videos = [];
    for (var i = 0; i < 5; i++) {
      // duplique 5 fois les videos
      for (var i = 0; i < 8; i++) {
        //ajoute le contenu au model
        videos.add(VideoModel(
            title: "Child Nation - Track ${i + 2}",
            assetChannel: "assets/c$i.PNG",
            assetVignette: "assets/p1.PNG",
            nameChannel: "Child N${i + 1}",
            views: "${4 + i} M vues il y'a $i mois"));
      }
    }
    return videos;
  }

  TextStyle style = TextStyle(color: Colors.white, fontWeight: FontWeight.bold);
  Color iconColor = Color.fromRGBO(169, 169, 169, 1);

  Widget leftIcon() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
          icon: Icon(Icons.menu, size: 25, color: Colors.white),
          onPressed: () {},
        ),
        Container(
          margin: EdgeInsets.only(left: 10),
          child: FlatButton(
            onPressed: () {},
            child: Row(
              children: [
                Image.asset("assets/logo.png", width: 26),
                Container(
                  width: 5,
                ),
                Text(
                  "YouTube",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                      letterSpacing: -0.5),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget rightIcon() {
    return Row(
      children: [
        Container(
          width: 60,
          child: FlatButton(
              onPressed: () {},
              child: Image.asset(
                "assets/img13.png",
                width: 50,
              )),
        ),
        Container(
          width: 60,
          child: FlatButton(
              onPressed: () {},
              child: Image.asset(
                "assets/img14.png",
                width: 50,
              )),
        ),
        Container(
          width: 60,
          child: FlatButton(
              onPressed: () {},
              child: Image.asset(
                "assets/img15.png",
                width: 50,
              )),
        ),
        loginButton()
      ],
    );
  }

  Widget loginButton() {
    return Container(
      padding: EdgeInsets.all(5),
      decoration:
          BoxDecoration(border: Border.all(color: Colors.blue, width: .5)),
      child: Row(
        children: [
          CircleAvatar(
            radius: 15,
            child: Icon(
              Icons.person,
              color: Color.fromRGBO(18, 18, 18, 1),
              size: 15,
            ),
          ),
          Container(width: 10),
          Text("SE CONNECTER",
              style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      drawer: Drawer(),
      backgroundColor: Color.fromRGBO(18, 18, 18, 1),
      appBar: PreferredSize(
        preferredSize: Size(width, height / 13.5),
        child: Container(
          height: height / 13.5,
          color: Color.fromRGBO(32, 32, 32, 1),
          child: Stack(children: [
            Positioned(
              top: 14,
              left: width / 4,
              child: Container(
                width: width / 2.7,
                height: 30,
                color: Color.fromRGBO(18, 18, 18, 1),
                child: TextFormField(
                  cursorColor: Colors.white,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w300),
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(top: 3, left: 10),
                    hintText: "Rechercher",
                    hintStyle:
                        TextStyle(color: Color.fromRGBO(117, 117, 117, 1)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(1)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(1),
                        borderSide: BorderSide(
                            color: Color.fromRGBO(28, 98, 185, 1), width: 1)),
                  ),
                ),
              ),
            ),
            Positioned(
                top: 14.8,
                right: width / 3,
                child: Container(
                  width: 70,
                  color: Color.fromRGBO(49, 49, 49, 1),
                  child: FlatButton(
                    onPressed: () {},
                    child: Icon(
                      Icons.search,
                      color: Color.fromRGBO(152, 152, 152, 1),
                    ),
                  ),
                )),
            Positioned(
              top: 8,
              child: leftIcon(),
              left: 20,
            ),
            Positioned(
              right: 20,
              // bottom: 0,
              top: -5,
              child: rightIcon(),
            )
          ]),
        ),
      ),
      body: Stack(
        children: [
          Positioned(
            left: 0,
            top: 0,
            child: menu(width, height),
          ),
          Positioned(
              top: 25,
              left: width / 5.7,
              child: Container(
                width: width / 1.25,
                height: height/ 1.08,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      videos(width, height),
                      divider(h: 3, color: iconColor.withOpacity(.5)),
                      space(20),
                      Text("Tendances", style: style.copyWith(fontSize: 25)),
                      space(20),
                      videos(width, height, l: 4),
                      divider(h: 3, color: iconColor.withOpacity(.5)),
                      space(20),
                      videos(width, height, l: 4),
                      divider(h: 3, color: iconColor.withOpacity(.5)),
                      space(100),
                      videos(width, height, l: 20),
                    ],
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Widget space(double h) {
    return SizedBox(height: h);
  }

  Widget items(
      {String title,
      String asset,
      bool icon = false,
      IconData myicon,
      Color color}) {
    String assetBase = "assets/";
    return ListTile(
      leading: icon
          ? Container(
              margin: EdgeInsets.only(left: 5),
              child: Icon(
                myicon,
                color: color,
              ),
            )
          : Image.asset(
              assetBase + asset,
              width: 33,
            ),
      title: Text(title,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w500, fontSize: 14)),
    );
  }

  Widget divider({h = .07, color = Colors.grey}) {
    return Container(
      height: h,
      color: color,
    );
  }

  Widget videos(double w, double h, {int l=8}) {
    return GridView.builder(
      shrinkWrap: true,
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
      itemCount: l,
      itemBuilder: (ctx, i) {
        return Video(video: getVideo()[i]);
      },
    );
  }

  Widget menu(double width, double height) {
    return Container(
      width: width / 6.4,
      height: height / 1.08,
      color: Color.fromRGBO(33, 33, 33, 1),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                color: Color.fromRGBO(56, 56, 56, 1),
                child: items(
                    title: "Accueil",
                    asset: "img1.png",
                    icon: true,
                    myicon: Icons.home,
                    color: Colors.white)),
            items(title: "Tendances", asset: "img2.png"),
            items(title: "Abonnements", asset: "img3.png"),
            divider(),
            items(title: "Bibliothèque", asset: "img2.png"),
            items(
                title: "Historique",
                icon: true,
                myicon: Icons.history,
                color: Color.fromRGBO(144, 144, 144, 1)),
            divider(),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Connectez-vous à YouTube pour cliquer sur J'aime , ajouter un commentaire et vous abonner.",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                  space(10),
                  loginButton()
                ],
              ),
            ),
            divider(),
            space(10),
            Text("LE MEILLEUR DE YOUTUBE",
                style: style.copyWith(color: Color.fromRGBO(169, 169, 169, 1))),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  items(title: "Music", asset: "img5.png"),
                  items(title: "Sport", asset: "img6.png"),
                  items(title: "Jeux vidéo", asset: "img7.png"),
                  items(title: "Actualités", asset: "img8.png"),
                  items(title: "En direct", asset: "img9.png"),
                  items(title: "Mode et beauté", asset: "img10.png"),
                  items(title: "Savoirs & cultures", asset: "img11.png"),
                  items(title: "Vidéo à 360°", asset: "img12.png"),
                ],
              ),
            ),
            space(15),
            divider(),
            space(5),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: items(
                  title: "Chaines",
                  icon: true,
                  myicon: Icons.add_circle,
                  color: Color.fromRGBO(169, 169, 169, 1)),
            ),
            divider(),
            space(10),
            Text("AUTRES CONTENUS YOUTUBE",
                style: style.copyWith(color: iconColor),
                textAlign: TextAlign.left),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: items(title: "En direct", asset: "img9.png"),
            ),
            divider(),
            space(10),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Column(
                children: [
                  items(
                      title: "Paramètre",
                      icon: true,
                      myicon: Icons.settings,
                      color: iconColor),
                  items(
                      title: "Histotique des signal...",
                      icon: true,
                      myicon: Icons.flag,
                      color: iconColor),
                  items(
                      title: "Aide",
                      asset: "img7.png",
                      icon: true,
                      myicon: Icons.help,
                      color: iconColor),
                  items(
                      title: "Envoyer des commen...",
                      icon: true,
                      myicon: Icons.message_rounded,
                      color: iconColor),
                ],
              ),
            ),
            space(10),
            divider(),
            space(10),
            Container(
              margin: EdgeInsets.only(left: 10),
              padding: EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "À propos Presse \nDroits d'auteur  Nous contacter\nCréateurs Publicité\nDéveloppeurs \n\nConditions d'utilisation \nConfidentialité\nRègles et sécurité\nPremiers pas sur YouTube\nTester de nouvelles fonctionnalités",
                    style: style.copyWith(
                        fontWeight: FontWeight.w500, color: iconColor),
                  ),
                  space(20),
                  Text('© 2020 Google LLC',
                      textAlign: TextAlign.start,
                      style: style.copyWith(
                        color: Colors.grey,
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                      )),
                  space(15),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
