class VideoModel {
  String title;
  String assetChannel;
  String nameChannel;
  String assetVignette;
  String views;
  VideoModel({this.assetChannel, this.assetVignette, this.nameChannel, this.title, this.views});
}