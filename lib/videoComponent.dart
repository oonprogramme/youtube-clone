import 'package:custom/videoModel.dart';
import 'package:flutter/material.dart';

class Video extends StatelessWidget {
  final VideoModel video;
  Video({this.video});
TextStyle style = TextStyle(color: Colors.white, fontWeight: FontWeight.bold);
  Color color = Color.fromRGBO(169, 169, 169, 1);
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
            child: Image.asset(video.assetVignette, width: 290),
            elevation: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                backgroundImage: AssetImage(video.assetChannel),
              ),
              SizedBox(width: 15,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(video.title, style: style.copyWith()),
                  SizedBox(height: 5),
                  Text(video.nameChannel, style: style.copyWith(fontWeight: FontWeight.w600, color: color)),
                  SizedBox(height: 5),
                  Text(video.views, style: style.copyWith(fontWeight: FontWeight.w600, color: color)),
                ],
              )
            ],
          ),
        ],
      ),
      Positioned(
        bottom: 170,
        right: 20,
        child: Container(
          padding: EdgeInsets.all(3),
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(.5),
            borderRadius: BorderRadius.circular(5)
          ),
          alignment: Alignment.centerRight,
          child: Text("4:31", style: style.copyWith(fontWeight: FontWeight.w600)),
        ),
      )
      ],
    );
  }
}